# BinaryGame Renovate Bot

This bot runs on a schedule to create merge request to update the dependencies on all of BinaryGame's repositories.

See more:
- [Renovate Bot](https://github.com/renovatebot/renovate)
- [Gitlab renovate runner](https://gitlab.com/renovate-bot/renovate-runner)

## Job logs

The logs for the jobs running in the pipeline are private for security reasons.
